package com.yuriy.client;

import com.yuriy.client.Context;
import com.yuriy.json.JsonValue;

import java.util.ArrayList;
import java.util.HashMap;

public interface Module {
    public void init(JsonValue parameters, Context api) throws Exception;
    public void run(Context api) throws Exception;
}
