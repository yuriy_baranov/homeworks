package com.yuriy;

import com.yuriy.client.BplaException;
import com.yuriy.core.Initializer;
import com.yuriy.json.JsonParser;
import com.yuriy.json.JsonValue;
import com.yuriy.logging.Level;
import com.yuriy.logging.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.UUID;

public class App
{
    public static void main( String[] args ) throws Exception
    {
        Logger logger = new Logger("System scheduler", Level.DEBUG, System.out);
        if (args.length != 1) {
            throw new BplaException("Expected single argument with config file path");
        }
        File configFile = new File(args[0]);
        if (!configFile.exists()) {
            throw new BplaException(String.format("File %s does not exist", configFile.getPath()));
        }
        JsonValue config = JsonParser.readJson(new FileInputStream(configFile));
        if (config.get("modules").isUndefined()) {
            throw new BplaException("Modules not found in config");
        }
        ArrayList<JsonValue> modules = config.get("modules").asList();
        JsonValue settings = config.get("settings");
        Initializer init = new Initializer(logger);
        if (settings.isDefined()) {
            if (settings.get("classpath").isList()) {
                ArrayList<JsonValue> libraries = settings.get("classpath").asList();
                for (JsonValue lib : libraries) {
                    init.addSoftwareLibrary(new File(lib.asString()));
                }
            }
            if (settings.get("remote_classpath").isList()) {
                JsonValue libPath = settings.getOrElse("tmp_path", new JsonValue("./"));
                if (!new File(libPath.asString()).exists() && !new File(libPath.asString()).mkdirs()) {
                    throw new BplaException(String.format("Cannot create temp directory: %s", libPath.asString()));
                }
                ArrayList<JsonValue> urls = settings.get("remote_classpath").asList();
                for (JsonValue url : urls) {
                    String name = String.format("lib_%d.jar", Math.abs(url.asString().hashCode()));
                    File lib = Paths.get(libPath.asString(), name).toFile();
                    if (!lib.exists()) {
                        init.downloadFile(url.asString(), lib);
                    }
                    init.addSoftwareLibrary(lib);
                }
            }
        }
        init.startModules(modules);
    }
}
