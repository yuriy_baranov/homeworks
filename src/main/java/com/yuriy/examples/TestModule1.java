package com.yuriy.examples;

import com.yuriy.client.BplaException;
import com.yuriy.client.Context;
import com.yuriy.client.Module;
import com.yuriy.json.JsonValue;
import com.yuriy.logging.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class TestModule1 implements Module {
    private String dataKey, sendKey;
    private Logger logger;

    @Override
    public void init(JsonValue parameters, Context api) throws BplaException {
        api.allocate("atomics", 5, AtomicInteger::new);
        logger = api.getLogger();
        Consumer<Object> handler = msg -> {
            logger.info("got message from module 2: " + msg);
            dataKey = (String) msg;
        };
        String receiveKey = parameters.get("module1_receive_key").asString();
        api.addMessageListener(receiveKey, handler);
        sendKey = parameters.get("module2_receive_key").asString();
        if (parameters.get("test_float").isDefined()) {
            logger.info("Test float in config: " + parameters.get("test_float").asFloat());
        }
        if (parameters.get("test_bool").isDefined()) {
            logger.info("Test bool in config: " + parameters.get("test_bool").asBool());
        }
    }

    @Override
    public void run(Context api) throws BplaException, InterruptedException {
        Thread.sleep(100);
        logger.info("Sending hello message, key: " + sendKey);
        api.sendMessage(sendKey, "Hello from module 1");
        for (Object item: api.allocate(dataKey, 0, null)) {
            logger.info("read item: " + item);
        }
        api.exit();
    }
}
