package com.yuriy.client;

import com.yuriy.logging.Logger;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Supplier;

public interface Context {
    public void sendMessage(String key, Object message) throws BplaException;
    public void addMessageListener(String key, Consumer<Object> func) throws BplaException;
    public void removeMessageListener(String key) throws BplaException;
    public <T> ArrayList<T> allocate(String key, int n, Supplier<? extends T> supplier) throws BplaException;
    public <T> ArrayList<T> get(String key) throws BplaException;
    public <T> void free(String key) throws BplaException;
    public void exit();
    public Logger getLogger();
}
