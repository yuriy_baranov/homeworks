package com.yuriy.json;

import com.yuriy.client.BplaException;

public class JsonException extends BplaException {
    public JsonException(String e) {
        super(e);
    }
}
