package com.yuriy.json;

import java.util.ArrayList;
import java.util.Map;

@SuppressWarnings("unchecked")
public class JsonValue {
    private Object value;
    private JsonType type;

    public JsonValue() {
        type = JsonType.UNDEFINED;
    }

    static JsonValue createNull() {
        JsonValue x = new JsonValue();
        x.type = JsonType.NULL;
        return x;
    }

    public JsonValue(JsonValue x) {
        this.value = x.value;
        this.type = x.type;
    }

    public JsonValue(Map<String, JsonValue> m) {
        if (m == null) {
            this.type = JsonType.NULL;
        } else {
            this.value = m;
            this.type = JsonType.MAP;
        }
    }

    public JsonValue(ArrayList<JsonValue> a) {
        if (a == null) {
            this.type = JsonType.NULL;
        } else {
            this.value = a;
            this.type = JsonType.LIST;
        }
    }

    public JsonValue(String s) {
        if (s == null) {
            this.type = JsonType.NULL;
        } else {
            this.value = s;
            this.type = JsonType.STRING;
        }
    }

    public JsonValue(Float x) {
        if (x == null) {
            this.type = JsonType.NULL;
        } else {
            this.value = x;
            this.type = JsonType.FLOAT;
        }
    }

    public JsonValue(Boolean x) {
        if (x == null) {
            this.type = JsonType.NULL;
        } else {
            this.value = x;
            this.type = JsonType.BOOL;
        }
    }

    public boolean isMap() {
        return this.type == JsonType.MAP;
    }

    public boolean isList() {
        return this.type == JsonType.LIST;
    }

    public boolean isString() {
        return this.type == JsonType.STRING;
    }

    public boolean isFloat() {
        return this.type == JsonType.FLOAT;
    }

    public boolean isNull() {
        return this.type == JsonType.NULL;
    }

    public boolean isBool() {
        return this.type == JsonType.BOOL;
    }

    public boolean isDefined() {
        return this.type != JsonType.UNDEFINED;
    }

    public boolean isUndefined() {
        return this.type == JsonType.UNDEFINED;
    }

    public ArrayList<JsonValue> asList() throws JsonException {
        if (!isList()) {
            throw new JsonException("Expected list, actual type: " + type);
        }
        return (ArrayList<JsonValue>) value;
    }

    public Map<String, JsonValue> asMap() throws JsonException {
        if (!isMap()) {
            throw new JsonException("Not a map, actual type: " + type);
        }
        return (Map<String, JsonValue>) value;
    }

    public String asString() throws JsonException {
        if (!isString()) {
            throw new JsonException("Not a string, actual type: " + type);
        }
        return (String) value;
    }

    public Integer asInt() throws JsonException {
        return Math.round(asFloat());
    }

    public Float asFloat() throws JsonException {
        if (!isFloat()) {
            throw new JsonException("Not a number, actual type: " + type);
        }
        return (Float) value;
    }

    public Boolean asBool() throws JsonException {
        if (!isBool()) {
            throw new JsonException("Not a boolean, actual type: " + type);
        }
        return (Boolean) value;
    }

    public JsonValue get(String key) throws JsonException {
        if (!isMap()) {
            throw new JsonException("Expected map, actual type: " + type);
        }
        Map<String, JsonValue> m = ((Map<String, JsonValue>) value);
        if (!m.containsKey(key)) {
            JsonValue x = new JsonValue();
            x.type = JsonType.UNDEFINED;
            return x;
        }
        return m.get(key);
    }

    public boolean contains(String key) throws JsonException {
        if (!isMap()) {
            throw new JsonException("Expected map, actual type: " + type);
        }
        return ((Map<String, JsonValue>) value).containsKey(key);
    }

    public JsonValue remove(String key) throws JsonException {
        if (!isMap()) {
            throw new JsonException("Expected map, actual type: " + type);
        }
        return ((Map<String, JsonValue>) value).remove(key);
    }

    public JsonValue put(String key, JsonValue v) throws JsonException {
        if (!isMap()) {
            throw new JsonException("Expected map, actual type: " + type);
        }
        return ((Map<String, JsonValue>) value).put(key, v);
    }

    public boolean isEmpty() throws JsonException {
        if (isMap()) {
            return ((Map<String, JsonValue>) value).isEmpty();
        } else if (isList()) {
            return ((ArrayList<JsonValue>) value).isEmpty();
        }
        throw new JsonException("Not a list and not a map, actual type: " + type);
    }

    public void add(JsonValue v) throws JsonException {
        if (!isList()) {
            throw new JsonException("Expected list, actual type: " + type);
        }
        ((ArrayList<JsonValue>) value).add(v);
    }

    public JsonValue get(int index) throws JsonException {
        if (!isMap()) {
            throw new JsonException("Expected list, actual type: " + type);
        }
        return ((ArrayList<JsonValue>) value).get(index);
    }

    public JsonValue getOrElse(String key, JsonValue defaultValue) throws JsonException {
        JsonValue v = get(key);
        return v.isDefined() ? v : defaultValue;
    }
}
