package com.yuriy.logging;

import java.io.PrintStream;

import static com.yuriy.logging.Level.*;


public class Logger {
    private String name;
    private Level level;
    private PrintStream stream = System.out;

    public Logger(String name, Level level, PrintStream stream) {
        this.name = name;
        this.level = level;
        if (stream != null) {
            this.stream = stream;
        }
    }

    public Logger(String name, Level level) {
        this(name, level, null);
    }
    
    private void print(Level msgLevel, String msg) {
        if (msgLevel.getValue() >= this.level.getValue()) {
            stream.println(String.format("%s (%s) %s", msgLevel.getName(), name, msg));
        }
    }

    public void debug(Object msg) {
        print(DEBUG, msg.toString());
    }

    public void info(Object msg) {
        print(INFO, msg.toString());
    }

    public void warn(Object msg) {
        print(WARN, msg.toString());
    }

    public void error(Object msg) {
        print(ERROR, msg.toString());
    }
}
