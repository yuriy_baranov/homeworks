package com.yuriy.net;

import com.yuriy.client.BplaException;
import com.yuriy.client.Context;
import com.yuriy.client.Module;
import com.yuriy.json.JsonValue;
import com.yuriy.logging.Logger;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Broadcaster implements Module {
    private ArrayList<InetSocketAddress> addrs = new ArrayList<>();
    private ArrayList<String> keys = new ArrayList<>();
    private int period = 0;
    private Logger logger;

    @Override
    public void init(JsonValue parameters, Context api) throws Exception {
        if (!parameters.isMap()) {
            throw new BplaException("Expected parameters as Map");
        }
        JsonValue keys = parameters.get("keys");
        if (!keys.isList()) {
            throw new BplaException("parameter keys must be list");
        }
        for (JsonValue key : keys.asList()) {
            if (!key.isString()) {
                throw new BplaException("Keys contains not a string");
            }
            this.keys.add(key.asString());
        }
        JsonValue addrs = parameters.get("addrs");
        for (JsonValue addr : addrs.asList()) {
            this.addrs.add(new InetSocketAddress(InetAddress.getByName(addr.get("ip").asString()), addr.get("port").asInt()));
        }
        if (parameters.contains("period")) {
            this.period = parameters.get("period").asInt();
        }
        logger = api.getLogger();
    }

    @Override
    public void run(Context api) throws Exception {
        ByteBuffer message = ByteBuffer.allocate(1000);
        while (true) {
            message.position(0);
            for (String key : keys) {
                ArrayList<AtomicInteger> data;
                try {
                    data = api.get(key);
                }
                catch (BplaException e) {
                    logger.error(String.format("Data for key %s not found", key));
                    continue;
                }
                long ts = System.currentTimeMillis();
                message.putLong(ts);
                byte[] keyBytes = key.getBytes();
                message.putInt(keyBytes.length);
                message.put(keyBytes);
                message.putInt(data.size());
                if (message.position() + data.size() > message.capacity()) {
                    logger.error(String.format("Too big message for key %s, skipping", key));
                    continue;
                }
                for (AtomicInteger x : data) {
                    message.putInt(x.get());
                }
                for (InetSocketAddress addr : addrs) {
                    DatagramSocket clientSocket = new DatagramSocket();
                    logger.info(String.format("Sending %d bytes to %s", message.position(), addr.toString()));
                    DatagramPacket sendPacket = new DatagramPacket(
                            message.array(), message.position(), addr.getAddress(), addr.getPort()
                    );
                    clientSocket.send(sendPacket);
                    clientSocket.close();
                }
            }
            Thread.sleep(period);
        }
    }
}
