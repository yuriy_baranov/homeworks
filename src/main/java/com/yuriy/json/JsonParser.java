package com.yuriy.json;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * Minimal json parser
 */
public class JsonParser {
    private String s;
    private int cur = 0;

    public JsonParser(String s) {
        this.s = s;
    }

    public static JsonValue readJson(InputStream is) throws IOException, JsonException {
        StringBuilder buffer = new StringBuilder();
        int c;
        while (is.available() > 0) {
            if (!Character.isWhitespace(c = is.read())) {
                buffer.append((char) c);
            }
        }
        String json = buffer.toString();
        JsonParser parser = new JsonParser(json);
        return parser.parseJson();
    }

    private JsonValue parseJson() throws JsonException {
        char c = s.charAt(cur);
        switch (s.charAt(cur)) {
            case 'f':
            case 't':
                return readBool();
            case 'n':
                return readNull();
            case '"':
                return new JsonValue(readEscapedString());
            case '{':
                return readMap();
            case '[':
                return readList();
            case '-':
            case '.':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                return readFloat();
            default:
                throw new JsonException("Incorrect start of json: " + s.substring(cur));
        }
    }

    private JsonValue readList() throws JsonException {
        int start = cur;
        cur++;
        if (cur >= s.length()) {
            throw new JsonException("Unexpected end of list: [");
        }
        if (s.charAt(cur) == ']') {
            return new JsonValue(new ArrayList<>());
        }
        JsonValue result = new JsonValue(new ArrayList<>());
        while (cur < s.length()) {
            result.add(parseJson());
            if (cur >= s.length()) {
                throw new JsonException("Unexpected end of list: " + s.substring(start, s.length()));
            }
            if (s.charAt(cur) == ']') {
                cur++;
                break;
            }
            if (s.charAt(cur) != ',') {
                throw new JsonException("Invalid list separator: " + s.charAt(cur));
            }
            cur++;
        }
        return result;
    }

    private JsonValue readMap() throws JsonException {
        int start = cur;
        cur++; // { open bracket
        if (cur >= s.length()) {
            throw new JsonException("Unexpected end of map: {");
        }
        // empty map
        if (s.charAt(cur) == '}') {
            return new JsonValue(new HashMap<>());
        }
        JsonValue result = new JsonValue(new HashMap<>());
        while (cur < s.length()) {
            String key = readEscapedString();
            if (cur >= s.length()) {
                throw new JsonException("Unexpected end of map: " + s.substring(start, s.length()));
            }
            if (s.charAt(cur) != ':') {
                throw new JsonException("Invalid key value separator: " + s.charAt(cur));
            }
            cur++;
            result.put(key, parseJson());
            if (cur >= s.length()) {
                throw new JsonException("Unexpected end of map: " + s.substring(start, s.length()));
            }
            if (s.charAt(cur) == '}') {
                cur++;
                break;
            }
            if (s.charAt(cur) != ',') {
                throw new JsonException("Invalid map items separator: " + s.charAt(cur));
            }
            cur++;
        }
        return result;
    }

    private JsonValue readBool() throws JsonException {
        if (s.charAt(cur) == 't') {
            readLiteral("true");
            return new JsonValue(true);
        } else {
            readLiteral("false");
            return new JsonValue(false);
        }
    }

    private JsonValue readNull() throws JsonException {
        readLiteral("null");
        return JsonValue.createNull();
    }

    private void readLiteral(String literal) throws JsonException {
        if (cur + literal.length() >= s.length() || !s.substring(cur, cur + literal.length()).equals(literal)) {
            throw new JsonException("Unexpected token: " + s.substring(cur, cur + literal.length()));
        }
        cur += literal.length();
    }

    private JsonValue readFloat() throws JsonException {
        int start = cur, end = s.length();
        for (int i = cur + 1; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == ',' || c == '}' || c == ']') {
                end = i;
                break;
            }
        }
        cur = end;
        try {
            return new JsonValue(Float.valueOf(s.substring(start, end)));
        }
        catch (NumberFormatException e) {
            throw new JsonException(e.getMessage());
        }
    }

    private String readEscapedString() throws JsonException {
        if (s.length() <= cur) {
            throw new JsonException("Empty token");
        }
        if (s.charAt(cur) != '"') {
            throw new JsonException("Incorrect start of string: " + s.charAt(cur));
        }
        StringBuilder buffer = new StringBuilder();
        for (cur++; cur < s.length(); cur++) {
            char c = s.charAt(cur);
            if (c == '"') {
                cur++;
                break; // end of string
            }
            if (c != '\\') {
                buffer.append(c); // unescaped char
                continue;
            }
            if (cur + 1 == s.length()) {
                throw new JsonException("Escaped char not found: " + s.substring(cur, cur + 10));
            }
            switch (s.charAt(cur + 1)) {
                case '\\':
                    buffer.append('\\');
                    break;
                case 't':
                    buffer.append('\t');
                    break;
                case 'n':
                    buffer.append('\n');
                    break;
                case '"':
                    buffer.append('"');
                    break;
                default:
                    throw new JsonException("Invalid escaped char: " + s.charAt(cur + 1));
            }
            cur += 1; // escaped char
        }
        return buffer.toString();
    }
}
