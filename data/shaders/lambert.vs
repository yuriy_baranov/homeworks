varying vec4 vPosition;

varying vec3 outColor;
varying vec2 outTexCoord;

void main(void)
{
    vec3 normal, v, lightDir;
    vec4 diffuse, ambient, specular, globalAmbient;
    float NdotL;
    vec4 light_result = vec4(0.0);
    float point_factor = 1;

    normal = normalize(gl_NormalMatrix * gl_Normal);
    v = vec3(gl_ModelViewMatrix * gl_Vertex);

    globalAmbient = gl_LightModel.ambient * gl_FrontMaterial.ambient;

    for (int i = 0; i < gl_MaxLights; ++i) {

        if (gl_LightSource[i].position.w != 0.0)
        {
            lightDir = normalize(vec3(gl_LightSource[i].position) - v);
            float distance = length(vec3(gl_Vertex) - vec3(gl_LightSource[i].position));
            point_factor = 1.0 / (gl_LightSource[i].constantAttenuation +
                        gl_LightSource[i].linearAttenuation * distance +
                        gl_LightSource[i].quadraticAttenuation * distance * distance);
        }
        else {
            lightDir = normalize(vec3(gl_LightSource[i].position));
        }
        NdotL = max(dot(normal, lightDir), 0.0);
        diffuse = gl_FrontMaterial.diffuse * gl_LightSource[i].diffuse;

        ambient = gl_FrontMaterial.ambient * gl_LightSource[i].ambient;
        //globalAmbient = gl_LightModel.ambient * gl_FrontMaterial.ambient;

        vec3 viewDirection = normalize(-v);
        vec3 reflectDirection = normalize(-reflect(lightDir, normal));

        light_result += (NdotL * diffuse + ambient) * point_factor;
    }

    gl_FrontColor = light_result + globalAmbient;

    outColor = gl_FrontColor.xyz;
    outTexCoord = gl_MultiTexCoord0.st;

    vPosition = gl_ProjectionMatrix * gl_ModelViewMatrix * vec4(gl_Vertex.xyz, 1.0);

    gl_Position = vPosition;
}