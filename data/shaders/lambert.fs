varying vec4 vPosition;

in vec3 ourColor;
in vec2 outTexCoord;

uniform sampler2D ourTexture;

void main (void)
{
    vec4 color = texture(ourTexture, outTexCoord) * gl_Color;

    float fog_bias = 0;
    float fog_denom = 100;
    //if (vPosition.z > fog_bias)
    {
        float coeff = (vPosition.z - fog_bias) / fog_denom;
        float mul = max(0.0, 1.0 - coeff);
        color.x *= mul;
        color.y *= mul;
        color.z *= mul;
        color.x += min(135.0 / 256.0, coeff * 135.0 / 256.0);
        color.y += min(206.0 / 256.0, coeff * 206.0 / 256.0);
        color.z += min(235.0 / 256.0, coeff * 235.0 / 256.0);
    }

    gl_FragColor = color;
}