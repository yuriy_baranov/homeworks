package com.yuriy.examples;

import com.yuriy.client.BplaException;
import com.yuriy.client.Context;
import com.yuriy.client.Module;
import com.yuriy.json.JsonValue;
import com.yuriy.logging.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;

public class TestModule2 implements Module {
    private String sendKey, dataKey;
    private Logger logger;

    @Override
    public void init(JsonValue parameters, Context api) throws BplaException {
        logger = api.getLogger();
        Consumer<Object> handler = msg -> logger.info("got message: " + msg);
        String receiveKey = parameters.get("module2_receive_key").asString();
        api.addMessageListener(receiveKey, handler);
        sendKey = parameters.get("module1_receive_key").asString();
        dataKey = parameters.get("data_key").asString();;
    }

    @Override
    public void run(Context api) throws BplaException {
        api.sendMessage(sendKey, dataKey);
        String item = "This string was passed through Context.allocate method by module 2";
        ArrayList<String> data = api.allocate(dataKey, 1, () -> item);
        data.add("Second element");
    }
}
