package com.yuriy.net;

import com.yuriy.client.BplaException;
import com.yuriy.client.Context;
import com.yuriy.client.Module;
import com.yuriy.json.JsonValue;
import com.yuriy.logging.Logger;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Listener implements Module {
    private InetSocketAddress addr;
    private Logger logger;
    private HashMap<String, Long> lastUpdated = new HashMap<>();

    @Override
    public void init(JsonValue parameters, Context api) throws Exception {
        if (!parameters.isMap()) {
            throw new BplaException("Expected parameters as Map");
        }
        InetAddress ip = InetAddress.getByName(parameters.get("addr").get("ip").asString());
        addr = new InetSocketAddress(ip, parameters.get("addr").get("port").asInt());
        logger = api.getLogger();
    }

    @Override
    public void run(Context api) throws Exception {
        DatagramSocket serverSocket = new DatagramSocket(addr);
        byte[] receiveData = new byte[1024];
        while (true) {
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);
            logger.info("Received message");
            ByteBuffer buf = ByteBuffer.wrap(receiveData);
            long ts = buf.getLong();
            logger.info("timestamp: " + ts);
            int keyLen = buf.getInt();
            byte[] keyBytes = new byte[keyLen];
            for (int i = 0; i < keyLen; i++) {
                keyBytes[i] = buf.get();
            }
            String key = new String(keyBytes);
            logger.info("Last timestamp: " + lastUpdated.get(key));
            logger.info("data key: " + key);
            if (lastUpdated.get(key) == null || ts - lastUpdated.get(key) > 0) {
                logger.info("Updating data");
                lastUpdated.put(key, ts);
                int count = buf.getInt();
                logger.info("count: " + count);
                ArrayList<AtomicInteger> data = api.allocate(key, count, AtomicInteger::new);
                for (int i = 0; i < count; i++) {
                    data.get(i).set(buf.getInt());
                }
                logger.info("Update finished successfully");
            } else {
                logger.info("Skipped update because of having newer version");
            }
        }
    }
}
