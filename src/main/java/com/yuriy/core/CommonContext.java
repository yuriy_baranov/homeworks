package com.yuriy.core;

import com.yuriy.client.BplaException;
import com.yuriy.client.Context;
import com.yuriy.logging.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Supplier;

enum CommonContext implements Context {
    INSTANCE;

    HashMap<String, Consumer<Object>> listeners = new HashMap<>();
    HashMap<String, HashSet<String>> listenersKeyToIds = new HashMap<>();
    HashMap<String, Object> chunks = new HashMap<>();
    private boolean exited = false;

    @Override
    public synchronized void sendMessage(String key, Object message) throws BplaException {
        if (key == null) {
            throw new BplaException("Key is null");
        }
        HashSet<String> listenersForKey = listenersKeyToIds.getOrDefault(key, new HashSet<>());
        for (String id : listenersForKey) {
            Consumer<Object> func = listeners.get(id + "\t" + key);
            func.accept(message);
        }
        if (key.equals("EXIT")) {
            exit();
        }
    }

    @Override
    public synchronized void addMessageListener(String key, Consumer<Object> func) throws BplaException {
        if (key == null) {
            throw new BplaException("Key is null");
        }
        if (listeners.containsKey(key)) {
            throw new BplaException("listener is already set");
        }
        listeners.putIfAbsent(key, func);
        listenersKeyToIds.putIfAbsent(key.split("\t")[1], new HashSet<>());
        HashSet<String> ids = listenersKeyToIds.get(key.split("\t")[1]);
        ids.add(key.split("\t")[0]);
    }

    @Override
    public synchronized void removeMessageListener(String key) throws BplaException {
        if (key == null) {
            throw new BplaException("Key is null");
        }
        listeners.remove(key);
        listenersKeyToIds.get(key.split("\t")[1]).remove(key);
    }

    @Override
    public synchronized <T> ArrayList<T> allocate(String key, int n, Supplier<? extends T> supplier) throws BplaException {
        if (key == null) {
            throw new BplaException("Key is null");
        }
        if (chunks.containsKey(key)) {
            return (ArrayList<T>) chunks.get(key);
        }
        ArrayList<T> elements = new ArrayList<T>();
        for (int i = 0; i < n; i++) {
            elements.add(supplier.get());
        }
        chunks.put(key, elements);
        return elements;
    }

    @Override
    public synchronized <T> ArrayList<T> get(String key) throws BplaException {
        if (key == null) {
            throw new BplaException("Key is null");
        }
        if (!chunks.containsKey(key)) {
            throw new BplaException("Data was not allocated, key: " + key);
        }
        return (ArrayList<T>) chunks.get(key);
    }

    @Override
    public synchronized void free(String key) throws BplaException {
        if (key == null) {
            throw new BplaException("Key is null");
        }
        if (chunks.containsKey(key)) {
            throw new NoSuchElementException("no such element for key: " + key);
        }
        chunks.remove(key);
    }

    @Override
    public synchronized void exit() {
        exited = true;
        this.notify();
    }

    public boolean isExit() {
        return exited;
    }

    @Override
    public Logger getLogger() {
        return null;
    }
}
