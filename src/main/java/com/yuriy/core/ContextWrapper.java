package com.yuriy.core;

import com.yuriy.client.BplaException;
import com.yuriy.client.Context;
import com.yuriy.logging.Logger;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Supplier;

class ContextWrapper implements Context {
    private String id;
    private Context mainContext;
    private Logger logger;

    ContextWrapper(String id, Context mainContext, Logger logger) {
        this.id = id;
        this.mainContext = mainContext;
        this.logger = logger;
    }

    @Override
    public void sendMessage(String key, Object message) throws BplaException {
        mainContext.sendMessage(key, message);
    }

    @Override
    public void addMessageListener(String key, Consumer<Object> func) throws BplaException {
        mainContext.addMessageListener(id + '\t' + key, func);
    }

    @Override
    public void removeMessageListener(String key) throws BplaException {
        mainContext.removeMessageListener(id + '\t' + key);
    }

    @Override
    public <T> ArrayList<T> allocate(String key, int n, Supplier<? extends T> supplier) throws BplaException {
        return mainContext.allocate(key, n, supplier);
    }

    @Override
    public <T> ArrayList<T> get(String key) throws BplaException {
        return mainContext.get(key);
    }

    @Override
    public <T> void free(String key) throws BplaException {
        mainContext.free(key);
    }

    @Override
    public void exit() {
        mainContext.exit();
    }

    @Override
    public Logger getLogger() {
        return logger;
    }
}
