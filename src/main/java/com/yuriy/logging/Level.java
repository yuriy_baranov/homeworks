package com.yuriy.logging;

public enum Level {
    DEBUG(0, "DEBUG"),
    INFO(1, "INFO"),
    WARN(2, "WARN"),
    ERROR(3, "ERROR");

    private int value;
    private String name;
    Level(int value, String name) {
        this.value = value;
        this.name = name;
    }

    int getValue() {
        return value;
    }

    String getName() {
        return name;
    }
}
