package com.yuriy.json;

public enum  JsonType {
    MAP, LIST, STRING, FLOAT, BOOL, NULL, UNDEFINED;
}
