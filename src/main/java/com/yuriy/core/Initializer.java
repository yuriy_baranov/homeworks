package com.yuriy.core;

import com.yuriy.client.BplaException;
import com.yuriy.client.Context;
import com.yuriy.client.Module;
import com.yuriy.json.JsonValue;
import com.yuriy.logging.Level;
import com.yuriy.logging.Logger;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;

public class Initializer {
    private Logger mainLogger;
    public Initializer(Logger mainLogger) {
        this.mainLogger = mainLogger;
    }

    public void startModules(ArrayList<JsonValue> modules) throws Exception {
        CommonContext ctx = CommonContext.INSTANCE;
        ArrayList<Thread> modulesThreads = new ArrayList<>();
        for (JsonValue module : modules) {
            String name = module.get("name").asString();
            try {
                Object m = Class.forName(name);
                if (!Module.class.isAssignableFrom((Class) m)) {
                    throw new BplaException("Module class must implement Module interface");
                }
                Module moduleInstance;
                try {
                    moduleInstance = (Module) ((Class) m).newInstance();
                }
                catch (Exception e) {
                    mainLogger.info(String.format("Cannot create instance of module %s. Exception: %s", name, e.getMessage()));
                    continue;
                }
                JsonValue level = module.get("logger_level");
                Level loggerLevel = level.isDefined() ? Level.valueOf(level.asString()) : Level.DEBUG;
                Logger moduleLogger = new Logger(name, loggerLevel, System.err);
                Context moduleContext = new ContextWrapper(name, ctx, moduleLogger);
                mainLogger.info("Initializing " + name);
                try {
                    moduleInstance.init(module.get("parameters"), moduleContext);
                }
                catch (Exception e) {
                    mainLogger.error("Exception in init: " + e.getMessage());
                    e.printStackTrace();
                    continue;
                }
                mainLogger.info("Finished initializing " + name);
                Thread t = new Thread(() -> {
                    try {
                        moduleInstance.run(moduleContext);
                    } catch (Exception e) {
                        mainLogger.error(String.format("Module %s failed with exception: " + e.getMessage(), name));
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        PrintStream ps = new PrintStream(out);
                        e.printStackTrace(ps);
                        mainLogger.error("Exception: " + e.getMessage());
                        mainLogger.error(out.toString());
                    }
                });
                modulesThreads.add(t);
            }
            catch (ClassNotFoundException e) {
                throw new BplaException(String.format("Module class not found: %s", name));
            }
        }
        if (modulesThreads.isEmpty()) {
            mainLogger.info("No modules successfully initialized, finishing");
            return;
        }
        for (Thread t : modulesThreads) {
            t.start();
        }
        try {
            mainLogger.info("Waiting for modules to send exit message");
            synchronized (ctx) {
                while (!ctx.isExit()) {
                    ctx.wait();
                }
            }
            mainLogger.info("Got exit message, finishing");
        }
        catch (InterruptedException e) {
            mainLogger.info("Interrupted, exiting");
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void addSoftwareLibrary(File file) throws Exception {
        mainLogger.info(String.format("Adding %s to classpath", file.getPath()));
        Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class[]{URL.class});
        method.setAccessible(true);
        method.invoke(ClassLoader.getSystemClassLoader(), new Object[]{file.toURI().toURL()});
    }

    public void downloadFile(String url, File outputFile) throws IOException {
        mainLogger.info("Downloading " + url);
        URL website = new URL(url);
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        FileOutputStream fos = new FileOutputStream(outputFile);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
    }
}
